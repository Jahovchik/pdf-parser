FROM openjdk:17-alpine
EXPOSE 8080
COPY target/*.jar app.jar

ENV TESSDATA_PREFIX=/usr/share/tessdata

RUN mkdir -p /usr/share/tessdata
ADD https://github.com/tesseract-ocr/tessdata_best/raw/main/eng.traineddata /usr/share/tessdata/eng.traineddata
ADD https://github.com/tesseract-ocr/tessdata_best/raw/main/rus.traineddata /usr/share/tessdata/rus.traineddata
ADD https://github.com/tesseract-ocr/tessdata_best/raw/main/osd.traineddata /usr/share/tessdata/osd.traineddata

RUN apk add tesseract-ocr

ENTRYPOINT ["java","-jar","/app.jar"]
