package com.example.pdfparser.model;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.util.Objects;

/**
 * The class represents detected fragment of text for specified query with a certain accuracy of searching.
 * Note: this class has a natural ordering that is inconsistent with equals.
 */
@NoArgsConstructor
@AllArgsConstructor
@Getter
@Setter
public class FragmentScore implements Comparable<FragmentScore> {

    private String query;

    private String fragment;

    private int score;

    @Override
    public int compareTo(FragmentScore other) {
        return Integer.compare(score, other.score);
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof FragmentScore)) return false;
        FragmentScore fragmentScore = (FragmentScore) o;
        return score == fragmentScore.score
                && Objects.equals(query, fragmentScore.query)
                && Objects.equals(fragment, fragmentScore.fragment);
    }

    @Override
    public int hashCode() {
        return Objects.hash(query, fragment, score);
    }

}
