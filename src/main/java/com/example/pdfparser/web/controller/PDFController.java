package com.example.pdfparser.web.controller;

import com.example.pdfparser.model.FragmentScore;
import com.example.pdfparser.service.FuzzySearchService;
import com.example.pdfparser.service.PDFParserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.servlet.mvc.method.annotation.StreamingResponseBody;

import java.io.IOException;
import java.io.InputStream;
import java.io.UncheckedIOException;

@RestController
@RequestMapping(path = "/pdf")
public class PDFController {

    private final PDFParserService pdfParserService;
    private final FuzzySearchService fuzzySearchService;

    @Autowired
    public PDFController(PDFParserService pdfParserService, FuzzySearchService fuzzySearchService) {
        this.pdfParserService = pdfParserService;
        this.fuzzySearchService = fuzzySearchService;
    }

    @PostMapping(path = "/parse")
    public ResponseEntity<StreamingResponseBody> parsePDF(@RequestParam("file") MultipartFile file) {
        try {
            InputStream inputStream = file.getInputStream();
            StreamingResponseBody responseBody = pdfParserService.getTextAsStreamingResponse(inputStream);
            return ResponseEntity.ok().contentType(MediaType.TEXT_PLAIN).body(responseBody);
        } catch (IOException e) {
            throw new UncheckedIOException("Unable to read request body", e);
        }
    }

    @PostMapping(path = "/search")
    public FragmentScore[] search(@RequestPart("queries") String[] queries, @RequestPart("file") MultipartFile file) {
        try {
            InputStream inputStream = file.getInputStream();
            String text = pdfParserService.getTextAsString(inputStream);
            FragmentScore[] scores = new FragmentScore[queries.length];
            for (int i = 0; i < queries.length; i++) {
                scores[i] = fuzzySearchService.getBestFragment(text, queries[i])
                        .orElse(new FragmentScore(queries[i], "", 0));
            }
            return scores;
        } catch (IOException e) {
            throw new UncheckedIOException("Unable to read request body", e);
        }
    }

}
