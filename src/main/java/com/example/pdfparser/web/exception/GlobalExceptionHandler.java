package com.example.pdfparser.web.exception;

import com.example.pdfparser.service.exception.CharacterRecognitionException;
import com.example.pdfparser.service.exception.IllegalDocumentStateException;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.context.request.WebRequest;
import org.springframework.web.servlet.mvc.method.annotation.ResponseEntityExceptionHandler;

import java.io.UncheckedIOException;

@ControllerAdvice
public class GlobalExceptionHandler extends ResponseEntityExceptionHandler {

    @ExceptionHandler(value = {UncheckedIOException.class, IllegalDocumentStateException.class, CharacterRecognitionException.class})
    protected final ResponseEntity<Object> handleBadRequest(RuntimeException ex, WebRequest request) {
        logger.warn("Bad request: " + ex.getMessage());
        logger.debug("Bad request: ", ex);

        HttpStatus status = HttpStatus.BAD_REQUEST;
        String message = ex.getMessage();
        return handleExceptionInternal(ex, message, new HttpHeaders(), status, request);
    }

}
