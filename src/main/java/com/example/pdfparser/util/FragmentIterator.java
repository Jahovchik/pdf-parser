package com.example.pdfparser.util;

public interface FragmentIterator {

    boolean hasNext();

    void next();

    String getCurrentFragment();

}
