package com.example.pdfparser.util;

/**
 * Class for iterating text in fragments of a given length. Length is specified in tokens.
 * Token is a sequence of non-whitespace symbols. Sequence means the presence of at least two adjacent
 * non-whitespace symbols. Fragments have intersection between themselves, i.e. there are some common tokens for
 * each pair of adjacent fragments.
 */
public class FragmentIteratorImpl implements FragmentIterator {

    private static final int FRAGMENT_LENGTH_IN_TOKENS = 25;
    private static final int ADDITIONAL_INTERSECTION_IN_TOKENS = 5;

    private final String text;
    private final int textLengthInSymbols;
    private final int fragmentIntersectionInTokens;
    private int indexStart = 0;
    private int indexEnd = 0;
    private int indexNextStart;

    public FragmentIteratorImpl(String text, int queryLengthInTokens) {
        this.text = text;
        this.textLengthInSymbols = text.length();
        this.fragmentIntersectionInTokens = queryLengthInTokens + ADDITIONAL_INTERSECTION_IN_TOKENS;
        indexNextStart = findTokenStart(indexStart);
    }

    /**
     * Returns current fragment in the iteration. To get the first fragment
     * at least one invocation of next() method is required.
     *
     * @return text fragment
     */
    public String getCurrentFragment() {
        if (indexEnd == 0) {
            throw new IllegalStateException("No current fragment. Use next() method");
        }
        return text.substring(indexStart, indexEnd + 1);
    }

    public boolean hasNext() {
        return indexEnd < (textLengthInSymbols - 1)
                && indexNextStart < (textLengthInSymbols - 1);
    }

    public void next() {
        if (!hasNext()) {
            throw new IllegalStateException("No more elements");
        }
        indexStart = indexNextStart;
        indexNextStart = findTokenStart(findEndOfFragment(indexStart, FRAGMENT_LENGTH_IN_TOKENS));
        indexEnd = findEndOfFragment(indexNextStart, fragmentIntersectionInTokens);
    }

    //Single character tokens are ignored,
    //When there's no more tokens, method returns index of last symbol in the text
    private int findTokenStart(int start) {
        while (start + 1 < textLengthInSymbols) {
            if (!Character.isWhitespace(text.charAt(start))
                    && !Character.isWhitespace(text.charAt(start + 1))) {
                return start;
            }
            start++;
        }
        return start;
    }

    private int findTokenEnd(int end) {
        while (end < textLengthInSymbols
                && !Character.isWhitespace(text.charAt(end))) {
            end++;
        }
        return --end;
    }

    private int findEndOfFragment(int start, int lengthInTokens) {
        int numberOfTokens = 0;
        int end = start;
        while (numberOfTokens < lengthInTokens) {
            end = findTokenEnd(start + 1);
            start = findTokenStart(end);
            if (start == textLengthInSymbols - 1) {
                return end;
            }
            numberOfTokens++;
        }
        return end;
    }

}
