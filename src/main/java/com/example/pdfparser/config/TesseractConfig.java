package com.example.pdfparser.config;

import net.sourceforge.tess4j.ITesseract;
import net.sourceforge.tess4j.Tesseract;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
public class TesseractConfig {

    private static final int AUTO_PAGE_SEG_OSD = 1;
    private static final int NEURAL_NETS_LSTM = 1;

    @Bean
    public ITesseract tesseract() {
        Tesseract tesseract = new Tesseract();
        tesseract.setPageSegMode(AUTO_PAGE_SEG_OSD);
        tesseract.setOcrEngineMode(NEURAL_NETS_LSTM);
        return tesseract;
    }

}
