package com.example.pdfparser;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class PDFParserApplication {

	public static void main(String[] args) {
		SpringApplication.run(PDFParserApplication.class, args);
	}

}
