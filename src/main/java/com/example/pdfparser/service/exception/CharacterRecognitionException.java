package com.example.pdfparser.service.exception;

public class CharacterRecognitionException extends RuntimeException {

    public CharacterRecognitionException(String message, Throwable cause) {
        super(message, cause);
    }
}
