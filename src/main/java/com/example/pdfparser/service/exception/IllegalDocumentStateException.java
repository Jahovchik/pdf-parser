package com.example.pdfparser.service.exception;

public class IllegalDocumentStateException extends RuntimeException {

    public IllegalDocumentStateException(String message, Throwable cause) {
        super(message, cause);
    }
}
