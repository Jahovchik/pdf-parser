package com.example.pdfparser.service;

import com.example.pdfparser.service.exception.IllegalDocumentStateException;
import lombok.extern.slf4j.Slf4j;
import org.apache.pdfbox.pdmodel.PDDocument;
import org.apache.pdfbox.preflight.parser.PreflightParser;
import org.apache.pdfbox.preflight.utils.ByteArrayDataSource;
import org.apache.pdfbox.rendering.PDFRenderer;
import org.apache.pdfbox.text.PDFTextStripper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.web.servlet.mvc.method.annotation.StreamingResponseBody;

import java.awt.image.BufferedImage;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.io.UncheckedIOException;
import java.nio.charset.StandardCharsets;
import java.util.StringJoiner;
import java.util.function.Consumer;

/**
 * Service for parsing pdf file represented by InputStream
 */
@Service
@Slf4j
public class PDFParserServiceImpl implements PDFParserService {

    private static final String LANG = "rus";
    private static final int DPI = 300;

    private final OCRService ocrService;

    @Autowired
    public PDFParserServiceImpl(OCRService ocrService) {
        this.ocrService = ocrService;
    }

    /**
     * Parses data from an InputStream of PDF to String
     *
     * @param inputStream InputStream of bytes of a PDF file
     * @return parsed text
     */
    public String getTextAsString(InputStream inputStream) {
        StringJoiner joiner = new StringJoiner(System.lineSeparator());
        writePagesToConsumer(inputStream, joiner::add);
        return joiner.toString();
    }

    /**
     * Parses data from an InputStream of PDF to stream of plain text
     *
     * @param inputStream InputStream of bytes of a PDF file
     * @return response body with stream of plain text to use as a return type of REST controller
     */
    public StreamingResponseBody getTextAsStreamingResponse(InputStream inputStream) {
        return outputStream -> writePagesToConsumer(inputStream, getStreamTextConsumer(outputStream));
    }

    private Consumer<String> getStreamTextConsumer(OutputStream outputStream) {
        return text -> {
            try {
                byte[] bytes = text.getBytes(StandardCharsets.UTF_8);
                outputStream.write(bytes);
                outputStream.flush();
            } catch (IOException e) {
                throw new UncheckedIOException("Unable to write bytes to OutputStream", e);
            }
        };
    }

    private void writePagesToConsumer(InputStream stream, Consumer<String> consumer) {
        try (InputStream inputStream = stream;
             PDDocument document = parsePDF(inputStream)) {
            PDFRenderer renderer = new PDFRenderer(document);
            int pageNumber = document.getNumberOfPages();
            for (int i = 0; i < pageNumber; i++) {
                processPage(renderer, i, consumer);
                log.info("Page {} of {} successfully processed", i + 1, pageNumber);
            }
        } catch (IOException e) {
            log.warn("Exception during resource closing ", e);
        }
    }

    private PDDocument parsePDF(InputStream inputStream) {
        try {
            PreflightParser preflightParser = new PreflightParser(new ByteArrayDataSource(inputStream));
            preflightParser.parse();
            log.info("PDF document successfully parsed");
            return preflightParser.getPDDocument();
        } catch (IOException e) {
            throw new IllegalDocumentStateException("Unable to parse pdf document", e);
        }
    }

    private void processPage(PDFRenderer renderer, int pageIndex, Consumer<String> consumer) {
        try {
            BufferedImage image = renderer.renderImageWithDPI(pageIndex, DPI);
            String pageText = ocrService.extractText(image, LANG);
            consumer.accept(pageText);
        } catch (IOException e) {
            throw new IllegalDocumentStateException("Unable to scan pdf document", e);
        }
    }

    private String extractText(PDDocument document, int pageIndex) {
        try {
            PDFTextStripper reader = new PDFTextStripper();
            reader.setStartPage(pageIndex);
            reader.setEndPage(pageIndex);
            return reader.getText(document);
        } catch (IOException e) {
            throw new IllegalDocumentStateException("Unable to read pdf document", e);
        }
    }

}
