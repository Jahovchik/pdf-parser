package com.example.pdfparser.service;

import com.example.pdfparser.model.FragmentScore;
import com.example.pdfparser.util.FragmentIterator;
import com.example.pdfparser.util.FragmentIteratorImpl;
import lombok.extern.slf4j.Slf4j;
import me.xdrop.fuzzywuzzy.FuzzySearch;
import org.springframework.stereotype.Service;

import java.util.*;

/**
 * Class for searching a specified query in a text.
 * Public methods return the most relevant fragments of text that contain the query.
 */
@Service
@Slf4j
public class FuzzySearchServiceImpl implements FuzzySearchService {

    private static final int RATIO_THRESHOLD = 80;
    private static final String SPLIT_BY_WHITESPACES = "\\s+";
    private static final String SPLIT_BY_SYMBOLS = "[^\\p{javaLetterOrDigit}]+";

    /**
     * Returns the most relevant fragment of a text for given query.
     *
     * @param text  text to search in
     * @param query phrase to search for the presence in the text
     * @return the best fragment inside the text with words from the query
     */
    public Optional<FragmentScore> getBestFragment(String text, String query) {
        List<String> tokens = getTokens(query, SPLIT_BY_SYMBOLS);
        if (tokens.isEmpty()) {
            return Optional.empty();
        }
        FragmentIterator iterator = new FragmentIteratorImpl(text, tokens.size());
        Queue<FragmentScore> bestScores = getFragmentScores(query, iterator, tokens);
        log.info("Document was searched for the presence of \"{}\"", query);
        return Optional.ofNullable(bestScores.peek());
    }

    private Queue<FragmentScore> getFragmentScores(String query, FragmentIterator iterator, List<String> tokens) {
        Queue<FragmentScore> bestScores = new PriorityQueue<>(Comparator.reverseOrder());
        while (iterator.hasNext()) {
            iterator.next();
            String fragment = iterator.getCurrentFragment();
            int averageRatio = getAverageRatioForFragment(fragment, tokens);
            if (averageRatio >= RATIO_THRESHOLD) {
                bestScores.offer(new FragmentScore(query, fragment, averageRatio));
            }
        }
        return bestScores;
    }

    private int getAverageRatioForFragment(String fragment, List<String> tokens) {
        int sum = 0;
        int count = 0;
        for (String token : tokens) {
            sum += getRatioForToken(fragment, token);
            count++;
        }
        return count == 0 ? 0 : sum / count;
    }

    private int getRatioForToken(String fragment, String token) {
        List<String> fragmentTokens = getTokens(fragment, SPLIT_BY_WHITESPACES);
        int maxRatio = 0;
        for (String fragmentToken : fragmentTokens) {
            maxRatio = Math.max(maxRatio, calculateRatio(fragmentToken, token));
        }
        return maxRatio;
    }

    private int calculateRatio(String fragmentToken, String token) {
        if (fragmentToken.length() > token.length()) {
            return FuzzySearch.partialRatio(fragmentToken, token);
        } else {
            return FuzzySearch.ratio(fragmentToken, token);
        }
    }

    private List<String> getTokens(String text, String splitOptionRegex) {
        text = text.toLowerCase();
        String[] allTokens = text.split(splitOptionRegex);
        List<String> tokens = new ArrayList<>();
        for (String token : allTokens) {
            if (token.length() > 1) {
                tokens.add(token);
            }
        }
        return tokens;
    }

}
