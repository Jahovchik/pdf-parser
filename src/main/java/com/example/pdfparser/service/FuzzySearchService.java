package com.example.pdfparser.service;

import com.example.pdfparser.model.FragmentScore;

import java.util.Optional;

public interface FuzzySearchService {

    Optional<FragmentScore> getBestFragment(String text, String query);

}
