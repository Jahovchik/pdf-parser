package com.example.pdfparser.service;

import org.springframework.web.servlet.mvc.method.annotation.StreamingResponseBody;

import java.io.InputStream;

public interface PDFParserService {

    StreamingResponseBody getTextAsStreamingResponse(InputStream inputStream);

    String getTextAsString(InputStream inputStream);

}
