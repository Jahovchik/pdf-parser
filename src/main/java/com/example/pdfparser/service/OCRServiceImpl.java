package com.example.pdfparser.service;

import com.example.pdfparser.service.exception.CharacterRecognitionException;
import net.sourceforge.tess4j.ITesseract;
import net.sourceforge.tess4j.TesseractException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.awt.image.BufferedImage;

/**
 * Service for extracting text from an image using OCR engine
 */
@Service
public class OCRServiceImpl implements OCRService {

    private final ITesseract tesseract;

    @Autowired
    public OCRServiceImpl(ITesseract tesseract) {
        this.tesseract = tesseract;
    }

    public String extractText(BufferedImage image, String lang) {
        try {
            tesseract.setLanguage(lang);
            return tesseract.doOCR(image);
        } catch (TesseractException e) {
            throw new CharacterRecognitionException("Unable to perform OCR", e);
        }
    }

}
