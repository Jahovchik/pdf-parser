package com.example.pdfparser.service;

import java.awt.image.BufferedImage;

public interface OCRService {

    String extractText(BufferedImage image, String lang);

}
